//
//  StorageData.swift
//  MyPlaces_TableView
//
//  Created by Dmitry on 1.12.20.
//

import RealmSwift
// нужно создать объект Реалм который будет предоставлять доступ к базе данных
let realm = try! Realm()

class StorageManager {
    
    static func saveObjects (_ place: Place) {    // метод для хранения объектов с типом Place
        try! realm.write {                        // реализовываем добавление объекта в базу
            realm.add(place)                      // в скобках объект который хотим добавить в базу
        }
    }
    
    static func deleteObject (_ place: Place) {   // метод для удаления объекта из БД
        try! realm.write {
            realm.delete(place)
        }
    }
}
