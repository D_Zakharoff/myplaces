//
//  MainViewController.swift
//  MyPlaces_TableView
//
//  Created by Dmitry on 23.11.20.
//

import UIKit
import RealmSwift

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // MARK: - Properties
    
    // через nil мы говорим что хотим отображать результаты поиска в том же  контроллере где основной контент
    private let searchController = UISearchController(searchResultsController: nil)
    private var places: Results<Place>!  // Results это автообновляемый тип контенера который возвращает запрашиваемые   объекты. Результаты всегда отображают текущее состояние хранилища в текущем потоке, работает в режиме реального времени, можем записывать в него и сразу же считывать. Является аналогом массива
    private var ascendingSorting = true
    private var filteredPlaces: Results<Place>!   // будет хранить отфильтрованные результаты поиска
    private var searchBarIsEmpty: Bool {
        guard let text = searchController.searchBar.text else {return false}
        return text.isEmpty
    }
    private var isFiltering: Bool {
        return searchController.isActive && !searchBarIsEmpty // серч бар активен и является не пустым
        
    }
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var reversedSortingButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        places = realm.objects(Place.self)
        
            // Setup search controller
        searchController.searchResultsUpdater = self // получателем инфы об изменении текста поиск строки будет наш класс
        searchController.obscuresBackgroundDuringPresentation = false // позволяет взаимодействовать с контроллером поиска как с основным
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController // интегрируем строку поиска в навигейшн бар
        definesPresentationContext = true // позволяет отпустить строку поиска при переходе на другой экран
        
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return filteredPlaces.count
        }
        return places.isEmpty ? 0 : places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell
     var place = Place()
        if isFiltering {
            place = filteredPlaces[indexPath.row]
        } else {
            place = places[indexPath.row]
        }
        
        cell.nameLabel?.text = place.name
        cell.locationLabel.text = place.location
        cell.typeLabel.text = place.type
        cell.placeImage.image = UIImage(data: place.imageData!)
        // делаем закругленными imageView в списке, плюс добрались к высоте ячейки
        cell.placeImage.layer.cornerRadius = cell.placeImage.frame.size.height / 2
        // чтобы изображ стало тоже круглым обрезаем его по границам imageView
        cell.placeImage?.clipsToBounds = true
        return cell
    }
    
    // MARK: - Table view delegate
    
    //    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return 85   // почему при удалении этого метода слетают констрейты, хотя высота ячейки прописана в стори!!!! разобрался, по сути щас этот метод тут не нужен
    //    }
    
    // реализовываем метод удаления объектов
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let place = places[indexPath.row]
            StorageManager.deleteObject(place)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    // тоже способ удаления
    //    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
    //        let place = places[indexPath.row]
    //        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (_, _, _) in
    //            StorageManager.deleteObject(place)
    //            tableView.deleteRows(at: [indexPath], with: .automatic)
    //        }
    //        return UISwipeActionsConfiguration(actions: [deleteAction])
    //    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetails" {
            // присваиваем индекс выбранной ячеки
            guard let indexPath = tableView.indexPathForSelectedRow else { return }
            // извлекаем объект конкретной выбранной ячейки
            var place = Place()
            if isFiltering {
                place = filteredPlaces[indexPath.row]
            } else {
                place = places[indexPath.row]
            }
            let newPlaceVC = segue.destination as! NewPlaceController
            newPlaceVC.currentPlace = place
            
        }
    }
    
    // MARK: - Actions
    
    @IBAction func sortSelectionByDayAndName(_ sender: UISegmentedControl) {
        sorting()
    }
    
    @IBAction func reversedSorting(_ sender: UIBarButtonItem) {
        ascendingSorting.toggle()      // метод toggle() меняет значение на противоположное
        if ascendingSorting {
            reversedSortingButton.image = #imageLiteral(resourceName: "AZ")
        } else {
            reversedSortingButton.image = #imageLiteral(resourceName: "ZA")
        }
        sorting()
    }
    
    //будет срабатывать при нажатии кнопки сэйв в ньюПлэйсКонтроллере, возвращение с того экрана на главный
    @IBAction func unwindSegue(segue: UIStoryboardSegue) {
        // создаем экземпляр класса NewPlaceController
        guard let newPlaceVC = segue.source as? NewPlaceController else { return }
        // вызываем метод которому присвоены введенные пользователем значения нового места 
        newPlaceVC.savePlace()
        // обновляем вью для отображения свед о новом месте
        tableView.reloadData()
    }
    
    private func sorting() {
        if segmentControl.selectedSegmentIndex == 0 {
            places = places.sorted(byKeyPath: "date", ascending: ascendingSorting)
        } else {
            places = places.sorted(byKeyPath: "name", ascending: ascendingSorting)
        }
        tableView.reloadData()
    }
}

// MARK: - Extensions

extension MainViewController: UISearchResultsUpdating {
    // обязательный метод для протокола UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    private func filterContentForSearchText(_ searchText: String) {
        filteredPlaces = places.filter("name CONTAINS[c] %@ OR location CONTAINS[c] %@", searchText, searchText) // заполняем отфильтрованными значениями CONTAINS[c] - не привязываемся при поиске к регистру ... %@ - должны подставить конкретная переменная... searchText - наша пременная.... синтаксис параметров поиска можно посмотреть в документации реалм Queries -> filtering... name CONTAINS[c] %@ OR location CONTAINS[c] %@ - означает что должны осущ поиск по полю имя и местополож и фильтровать данные будем из параметра searchText внезавис от регистра символов
        tableView.reloadData()
        
    }
    
    
}
