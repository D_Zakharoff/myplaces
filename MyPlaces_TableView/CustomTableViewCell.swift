//
//  CustomTableViewCell.swift
//  MyPlaces_TableView
//
//  Created by Dmitry on 24.11.20.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var placeImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
}
