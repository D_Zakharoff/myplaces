//
//  NewPlaceController.swift
//  MyPlaces_TableView
//
//  Created by Dmitry on 27.11.20.
//

import UIKit

class NewPlaceController: UITableViewController {
    
    // MARK: - Properties
    var imageIsChanged = false
    // создали для того чтобы в него передавать объект из главного меню при редактировании
    var currentPlace: Place?
    
    
    // MARK: - Outlets
    @IBOutlet weak var placeImage: UIImageView!
    @IBOutlet weak var placeName: UITextField!
    @IBOutlet weak var placeLocation: UITextField!
    @IBOutlet weak var placeType: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // убираем разлиновку ненужных строк  ( после поля type)
        tableView.tableFooterView = UIView()
        // дб отключена по умолчанию, пока не введем что-то в placeName
        saveButton.isEnabled = false
        // будет срабатывать каждый раз при редактир placeName и вызывать метод textFieldChanged, котор будет следить ввели мы что-то или нет, чтоб делать видимой saveButton
        placeName.addTarget(self, action: #selector(textFieldChanged), for: .editingChanged)
        
        setupEditScreen()
    }
    
    // MARK: - Table view delegate
    
    // при индексе 0 (1 ячейка, там где фото) мы дб вызвать меню, в ином случае скрыть клаву
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            
            let photoIcon = #imageLiteral(resourceName: "photo")
            let cameraIcon = #imageLiteral(resourceName: "camera")
            let alert = UIAlertController(title: nil,
                                          message: nil,
                                          preferredStyle: .actionSheet)
            
            let camera = UIAlertAction(title: "Camera", style: .default) { _ in
                self.chooseImagePicker(source: .camera)
            }
            
            let photo = UIAlertAction(title: "Photo", style: .default) { _ in
                self.chooseImagePicker(source: .photoLibrary)
            }
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel)
            
            // имея фото добавляем их к конкр меню, но почему ключ именно такой
            camera.setValue(cameraIcon, forKey: "image")
            // надпись "camera" сдвигаем влево к иконке камеры
            camera.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            // имея фото добавляем их к конкр меню
            photo.setValue(photoIcon, forKey: "image")
            // надпись "camera" сдвигаем влево к иконк фото
            photo.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            
            alert.addAction(camera)
            alert.addAction(photo)
            alert.addAction(cancel)
            present(alert, animated: true)
        } else {
            view.endEditing(true)
        }
    }
    
    // метод который присваивает введенные пользователем значения в сооотв поля и сохраняет в БД
    func savePlace() {
        
        var image:UIImage?
        
        if imageIsChanged {
            image = placeImage.image
        } else {
            image = #imageLiteral(resourceName: "imagePlaceholder")
        }
        // привели к типу Data
        let imageData = image?.pngData()
        let newPlace = Place(name: placeName.text!,
                             location: placeLocation.text,
                             type: placeType.text,
                             imageData: imageData)
        
        if currentPlace != nil {
            try! realm.write {
                currentPlace?.name = newPlace.name
                currentPlace?.location = newPlace.location
                currentPlace?.type = newPlace.type
                currentPlace?.imageData = newPlace.imageData
            }
        } else {
            //указываем какой объект сохраняем в БД
            StorageManager.saveObjects(newPlace)
        }
    }
    
    // MARK: - Actions
    // метод привязан к кнопке кэнсел, принаж на котор будем выходить из контроллера и выгр все из памяти
    @IBAction func cancelAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    // реализуем метод который будет работать с переданными объектами из мэйнвьюконтроллера через сигвэй
    private func setupEditScreen () {
        
        if currentPlace != nil{  // если есть что-то то тогда редактируем
            // приводим тип ДАТА передаваемого из ячейки изображ в UIImage
            
            setupNavigationBar()
            imageIsChanged = true
            
            guard let dataImage = currentPlace?.imageData, let image = UIImage(data: dataImage) else {return}
            placeImage.image = image
            placeImage.contentMode = .scaleAspectFill
            placeName.text = currentPlace?.name
            placeLocation.text = currentPlace?.location
            placeType.text = currentPlace?.type
        }
    }
    
    private func setupNavigationBar () {
        if let topItem = navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
        // убираем кнопку cancel
        navigationItem.leftBarButtonItem = nil
        title = currentPlace?.name
        saveButton.isEnabled = true
    }
}

// MARK: - Text field delegate
// до этого в сториборде перетягиваем лучик с текстфилда на левую точку сонтроллера (из трех, справа - ехит) и выбираем велегейт
extension NewPlaceController: UITextFieldDelegate {
    
    // скрываем клавиатуру по нажатию на Done на клаве
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func textFieldChanged () {
        if placeName.text?.isEmpty == false {
            saveButton.isEnabled = true
        } else {
            saveButton.isEnabled = false
        }
    }
}
// MARK: - Work with adding image
// подпис на протокол чтоб могли реализ метод имедж пикер контроллер
extension NewPlaceController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func chooseImagePicker(source: UIImagePickerController.SourceType) {
        if UIImagePickerController.isSourceTypeAvailable(source) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self // imagePicker делегирует метод, а выполнять будет наш класс
            imagePicker.allowsEditing = true // расзрешаем пользователю редактировать изображение
            imagePicker.sourceType = source
            present(imagePicker, animated: true)
        }
    }
    // в этом методе присваеваем аутлету placeImage выбранное пользователем изобр
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        placeImage.image = info[.editedImage] as? UIImage
        placeImage.contentMode = .scaleAspectFill    // заполняем вью
        placeImage.clipsToBounds = true // обрезаем лишнее за вью
        imageIsChanged = true 
        dismiss(animated: true) // закрываем imagePickerController
    }
}


