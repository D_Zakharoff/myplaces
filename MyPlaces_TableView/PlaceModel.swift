//
//  PlaceModel.swift
//  MyPlaces_TableView
//
//  Created by Dmitry on 25.11.20.
//

import RealmSwift

class Place: Object {
    @objc dynamic var name = ""
    @objc dynamic var location: String?
    @objc dynamic var type: String?
    @objc dynamic var imageData: Data? // тип UIImage не поддерживается в realm, поэтому тип Data
    @objc dynamic var date = Date()
    
    // convenience предназнач чтоб полностью инициал все свойства класса, такого вида инициализатор должен вызывать
    // инициализатор самого класса с пустыми параметрами для того чтобы мы инициализировали все свойства значениями по умолчанию
    convenience init(name: String, location: String?, type: String?, imageData: Data?) {
        self.init()     // попросить Мишу объяснить про convenience и self.init()
        self.name = name
        self.location = location
        self.type = type
        self.imageData = imageData
    }
}
